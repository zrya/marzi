/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"
#include <string.h>

#include <glib.h>
#include <glib/gi18n-lib.h>

#include "marzi-window.h"
#include "marzi-resources.h"

#include "pages/accessibility/marzi-page-accessibility.h"
#include "pages/appearance/marzi-page-appearance.h"
#include "pages/dock/marzi-page-dock.h"
#include "pages/privacy/marzi-page-privacy.h"
#include "pages/windows/marzi-page-windows.h"
#include "pages/welcome/marzi-page-welcome.h"
#include "pages/done/marzi-page-done.h"


struct _MarziWindow
{
  AdwApplicationWindow       parent_instance;

  AdwCarousel               *carousel;
  GtkButton                 *button_previous;
  GtkButton                 *button_next;
  GtkButton                 *button_about;
  MarziPageDock             *dock_page;
};

G_DEFINE_FINAL_TYPE (MarziWindow, marzi_window, ADW_TYPE_APPLICATION_WINDOW)

static void
carousel_update_buttons (MarziWindow *self)
{
	gdouble position = adw_carousel_get_position (self->carousel);
	guint n_pages = adw_carousel_get_n_pages (self->carousel);
  
  gtk_widget_set_visible (GTK_WIDGET (self->button_previous), position < n_pages && position > 0);
  gtk_widget_set_visible (GTK_WIDGET (self->button_next), position < n_pages -1 && position > 0); 
  gtk_widget_set_visible (GTK_WIDGET (self->button_about), position == 0);
}

static void
carousel_button_next_clicked_cb (MarziWindow *self)
{
  gdouble current_index;

  current_index = adw_carousel_get_position (self->carousel);
  current_index++;
  adw_carousel_scroll_to (self->carousel, adw_carousel_get_nth_page (self->carousel, (guint)current_index), TRUE);
}

static void
carousel_button_previous_clicked_cb (MarziWindow *self)
{
  gdouble current_index;

  current_index = adw_carousel_get_position (self->carousel);
  current_index--;
  adw_carousel_scroll_to (self->carousel, adw_carousel_get_nth_page (self->carousel, (guint)current_index), TRUE);
}

static void
marzi_window_class_init (MarziWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/marzi-window.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziWindow, button_next);
  gtk_widget_class_bind_template_child (widget_class, MarziWindow, button_previous);
  gtk_widget_class_bind_template_child (widget_class, MarziWindow, button_about);
  gtk_widget_class_bind_template_child (widget_class, MarziWindow, carousel);
  gtk_widget_class_bind_template_child (widget_class, MarziWindow, dock_page);

  gtk_widget_class_bind_template_callback (widget_class, carousel_update_buttons);

  gtk_widget_class_install_action (widget_class, "carousel.next", NULL, (GtkWidgetActionActivateFunc) carousel_button_next_clicked_cb);
  gtk_widget_class_install_action (widget_class, "carousel.previous", NULL, (GtkWidgetActionActivateFunc) carousel_button_previous_clicked_cb);

  g_type_ensure (MARZI_TYPE_PAGE_ACCESSIBILITY);
  g_type_ensure (MARZI_TYPE_PAGE_APPEARANCE);
  g_type_ensure (MARZI_TYPE_PAGE_DOCK);
  g_type_ensure (MARZI_TYPE_PAGE_PRIVACY);
  g_type_ensure (MARZI_TYPE_PAGE_WINDOWS);
  g_type_ensure (MARZI_TYPE_PAGE_WELCOME);
  g_type_ensure (MARZI_TYPE_PAGE_DONE);
  
}

static void
marzi_window_init (MarziWindow *self)
{
  g_resources_register (marzi_get_resource ());
  gtk_widget_init_template (GTK_WIDGET (self));

  g_auto(GStrv) session = NULL;
  const gchar *session_list = g_getenv ("XDG_CURRENT_DESKTOP");

  if (session_list != NULL)
    session = g_strsplit (session_list, ":", -1);

  if (session != NULL && g_strcmp0(session[0], "GNOME") == 0)
  {
    adw_carousel_remove (self->carousel, GTK_WIDGET (self->dock_page));
  }
}