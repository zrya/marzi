/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_WINDOW (marzi_window_get_type())

G_DECLARE_FINAL_TYPE (MarziWindow, marzi_window, MARZI, WINDOW, AdwApplicationWindow)

G_END_DECLS
