/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-privacy.h"

#include "config.h"
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>

#define PRIVACY_PATH_ID "org.gnome.desktop.privacy"
#define LOCATION_PATH_ID "org.gnome.system.location"


struct _MarziPagePrivacy
{
  AdwBin               parent_instance;

  AdwSwitchRow        *camera_row;
  AdwSwitchRow        *location_row;
  AdwSwitchRow        *microphone_row;
  AdwSwitchRow        *usage_row;
  AdwSwitchRow        *report_row;

  GSettings           *privacy_settings;
  GSettings           *location_settings;
};

G_DEFINE_TYPE (MarziPagePrivacy, marzi_page_privacy, ADW_TYPE_BIN)

static void
marzi_page_privacy_dispose (GObject *object)
{
  MarziPagePrivacy *self = (MarziPagePrivacy *)object;

  g_clear_object (&self->privacy_settings);
  g_clear_object (&self->location_settings);

  G_OBJECT_CLASS (marzi_page_privacy_parent_class)->dispose (object);
}

static void
marzi_page_privacy_class_init (MarziPagePrivacyClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_page_privacy_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/privacy/marzi-page-privacy.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPagePrivacy, camera_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPagePrivacy, location_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPagePrivacy, microphone_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPagePrivacy, usage_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPagePrivacy, report_row);
}

static void
marzi_page_privacy_init (MarziPagePrivacy *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->privacy_settings = g_settings_new (PRIVACY_PATH_ID);
  self->location_settings = g_settings_new (LOCATION_PATH_ID);

  /* Privacy settings */

  g_settings_bind (self->privacy_settings, "disable-camera",
                   self->camera_row, "active",
                   G_SETTINGS_BIND_INVERT_BOOLEAN);

  g_settings_bind (self->privacy_settings, "disable-microphone",
                   self->microphone_row, "active",
                   G_SETTINGS_BIND_INVERT_BOOLEAN);

  g_settings_bind (self->privacy_settings, "send-software-usage-stats",
                   self->usage_row, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->privacy_settings, "report-technical-problems",
                   self->report_row, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->location_settings, "enabled",
                   self->location_row, "active",
                   G_SETTINGS_BIND_DEFAULT);
}