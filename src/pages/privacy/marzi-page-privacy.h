/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_PRIVACY (marzi_page_privacy_get_type ())

G_DECLARE_FINAL_TYPE (MarziPagePrivacy, marzi_page_privacy, MARZI, PAGE_PRIVACY, AdwBin)

G_END_DECLS
