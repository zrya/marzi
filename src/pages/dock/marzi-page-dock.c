/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-dock.h"

#include "config.h"
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>

#define DOCK_PATH_ID "org.gnome.shell.extensions.dash-to-dock"

struct _MarziPageDock
{
  AdwBin               parent_instance;

  GtkCheckButton      *dock_mode_toggle;
  GtkCheckButton      *panel_mode_toggle;
  AdwSwitchRow        *dock_autohide_row;
  AdwComboRow         *dock_position_row;
  GtkPicture          *dock_picture;
  GtkPicture          *panel_picture;

  GSettings           *dock_settings;
};

G_DEFINE_TYPE (MarziPageDock, marzi_page_dock, ADW_TYPE_BIN)

static void
on_option_click_released_cb (GtkGestureClick *self,
                             gint             n_press,
                             gdouble          x,
                             gdouble          y,
                             GtkCheckButton  *check_button)
{
  gtk_widget_activate (GTK_WIDGET (check_button));
}

static void
marzi_page_dock_dispose (GObject *object)
{
  MarziPageDock *self = (MarziPageDock *)object;

  g_clear_object (&self->dock_settings);

  G_OBJECT_CLASS (marzi_page_dock_parent_class)->dispose (object);
}

static void
on_dock_position_row_changed (MarziPageDock *self)
{
  int selected_position;

  selected_position = adw_combo_row_get_selected (self->dock_position_row);
  if (selected_position == 0)
  {
    gtk_picture_set_resource (self->dock_picture,"/org/zarya/marzi/pages/dock/assets/left/dock.svg");
    gtk_picture_set_resource (self->panel_picture,"/org/zarya/marzi/pages/dock/assets/left/panel.svg");
  }
  else if (selected_position == 1)
  {
    gtk_picture_set_resource (self->dock_picture,"/org/zarya/marzi/pages/dock/assets/bottom/dock.svg");
    gtk_picture_set_resource (self->panel_picture,"/org/zarya/marzi/pages/dock/assets/bottom/panel.svg");
  }
  else if (selected_position == 2)
  {
    gtk_picture_set_resource (self->dock_picture,"/org/zarya/marzi/pages/dock/assets/right/dock.svg");
    gtk_picture_set_resource (self->panel_picture,"/org/zarya/marzi/pages/dock/assets/right/panel.svg");
  }
}

static void
marzi_page_dock_class_init (MarziPageDockClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_page_dock_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/dock/marzi-page-dock.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, dock_mode_toggle);
  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, panel_mode_toggle);
  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, dock_autohide_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, dock_picture);
  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, panel_picture);
  gtk_widget_class_bind_template_child (widget_class, MarziPageDock, dock_position_row);

  gtk_widget_class_bind_template_callback (widget_class, on_dock_position_row_changed);
  gtk_widget_class_bind_template_callback (widget_class, on_option_click_released_cb);
}

typedef enum
{
  GSD_UBUNTU_DOCK_POSITION_TOP,
  GSD_UBUNTU_DOCK_POSITION_RIGHT,
  GSD_UBUNTU_DOCK_POSITION_BOTTOM,
  GSD_UBUNTU_DOCK_POSITION_LEFT,

  GSD_UBUNTU_DOCK_POSITION_FIRST = GSD_UBUNTU_DOCK_POSITION_RIGHT,
} GsdUbuntuDockPosition;

static GsdUbuntuDockPosition
get_dock_position_for_direction (MarziPageDock         *self,
                                 GsdUbuntuDockPosition  position)
{
  if (gtk_widget_get_state_flags (GTK_WIDGET (self)) & GTK_STATE_FLAG_DIR_RTL)
    {
      switch (position)
        {
          case GSD_UBUNTU_DOCK_POSITION_RIGHT:
            position = GSD_UBUNTU_DOCK_POSITION_LEFT;
            break;
          case GSD_UBUNTU_DOCK_POSITION_LEFT:
            position = GSD_UBUNTU_DOCK_POSITION_LEFT;
            break;
          default:
            break;
        }
    }

  return position;
}

static const char *
get_dock_position_string (GsdUbuntuDockPosition  position)
{
  switch (position)
    {
      case GSD_UBUNTU_DOCK_POSITION_TOP:
        return "TOP";
      case GSD_UBUNTU_DOCK_POSITION_RIGHT:
        return "RIGHT";
      case GSD_UBUNTU_DOCK_POSITION_BOTTOM:
        return "BOTTOM";
      case GSD_UBUNTU_DOCK_POSITION_LEFT:
        return "LEFT";
      default:
        g_return_val_if_reached ("LEFT");
    }
}

static GsdUbuntuDockPosition
get_dock_position_from_string (const char *position)
{
  if (g_str_equal (position, "TOP"))
    return GSD_UBUNTU_DOCK_POSITION_TOP;

  if (g_str_equal (position, "RIGHT"))
    return GSD_UBUNTU_DOCK_POSITION_RIGHT;

  if (g_str_equal (position, "BOTTOM"))
    return GSD_UBUNTU_DOCK_POSITION_BOTTOM;

  if (g_str_equal (position, "LEFT"))
    return GSD_UBUNTU_DOCK_POSITION_LEFT;

  g_return_val_if_reached (GSD_UBUNTU_DOCK_POSITION_LEFT);
}

static GsdUbuntuDockPosition
get_dock_position_row_position (MarziPageDock *self,
                                int            index)
{
  GListModel *model = adw_combo_row_get_model (self->dock_position_row);
  g_autoptr(GObject) value_object = g_list_model_get_item (model, index);

  return GPOINTER_TO_INT (g_object_get_data (G_OBJECT (value_object), "position"));
}

static int
get_dock_position_row_index (MarziPageDock         *self,
                             GsdUbuntuDockPosition  position)
{
  GListModel *model = adw_combo_row_get_model (self->dock_position_row);
  guint n_items;
  guint i;

  n_items = g_list_model_get_n_items (model);

  if (position == GSD_UBUNTU_DOCK_POSITION_TOP)
    return n_items;

  for (i = 0; i < n_items; i++)
    {
      g_autoptr(GObject) value_object = g_list_model_get_item (model, i);
      GsdUbuntuDockPosition item_position;

      item_position = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (value_object), "position"));

      if (item_position == position)
        return i;
    }

  g_return_val_if_reached (n_items);
}

static gboolean
dock_position_get_mapping (GValue   *value,
                           GVariant *variant,
                           gpointer  user_data)
{
  MarziPageDock *self = user_data;
  GsdUbuntuDockPosition position;

  position = get_dock_position_from_string (g_variant_get_string (variant, NULL));
  position = get_dock_position_for_direction (self, position);

  if (G_VALUE_TYPE (value) == G_TYPE_UINT)
    {
      g_value_set_uint (value, get_dock_position_row_index (self, position));
      return TRUE;
    }
  else if (G_VALUE_TYPE (value) == G_TYPE_STRING)
    {
      g_value_set_string (value, get_dock_position_string (position));
      return TRUE;
    }

  return FALSE;
}

static GVariant *
dock_position_set_mapping (const GValue       *value,
                           const GVariantType *type,
                           gpointer            user_data)
{
  MarziPageDock *self = user_data;
  GsdUbuntuDockPosition position;

  position = get_dock_position_row_position (self, g_value_get_uint (value));
  position = get_dock_position_for_direction (self, position);

  return g_variant_new_string (get_dock_position_string (position));
}

static void
populate_dock_position_row (AdwComboRow *combo_row)
{
  g_autoptr (GtkStringList) string_list = NULL;
  struct {
    char *name;
    GsdUbuntuDockPosition position;
  } positions[] = {
    {
      NC_("Position on screen for the Ubuntu dock", "Left"),
          GSD_UBUNTU_DOCK_POSITION_LEFT,
    },
    {
      NC_("Position on screen for the Ubuntu dock", "Bottom"),
          GSD_UBUNTU_DOCK_POSITION_BOTTOM,
    },
    {
      NC_("Position on screen for the Ubuntu dock", "Right"),
          GSD_UBUNTU_DOCK_POSITION_RIGHT,
    },
  };
  guint i;

  string_list = gtk_string_list_new (NULL);
  for (i = 0; i < G_N_ELEMENTS (positions); i++)
    {
      g_autoptr (GObject) value_object = NULL;

      gtk_string_list_append (string_list, _(positions[i].name));
      value_object = g_list_model_get_item (G_LIST_MODEL (string_list), i);
      g_object_set_data (value_object, "position",
                         GUINT_TO_POINTER (positions[i].position));
    }

  adw_combo_row_set_model (combo_row, G_LIST_MODEL (string_list));
}
static void
marzi_page_dock_init (MarziPageDock *self)
{

  gtk_widget_init_template (GTK_WIDGET (self));

  GSettingsSchemaSource *schema_source = g_settings_schema_source_get_default ();
  g_autoptr(GSettingsSchema) schema = NULL;

  schema = g_settings_schema_source_lookup (schema_source,
                                            DOCK_PATH_ID,
                                            TRUE);

  if (schema)
  {
    self->dock_settings = g_settings_new_full (schema, NULL, NULL);

    /* Dock settings */
    populate_dock_position_row (self->dock_position_row);
      
    g_settings_bind_with_mapping (self->dock_settings, "dock-position",
                                  self->dock_position_row, "selected",
                                  G_SETTINGS_BIND_DEFAULT,
                                  dock_position_get_mapping,
                                  dock_position_set_mapping,
                                  self, NULL);

    if (g_settings_get_boolean (self->dock_settings, "extend-height"))
      gtk_check_button_set_active (self->panel_mode_toggle, TRUE);
    else
      gtk_check_button_set_active (self->dock_mode_toggle, TRUE);

    g_settings_bind (self->dock_settings, "extend-height",
                    self->panel_mode_toggle, "active",
                    G_SETTINGS_BIND_DEFAULT);

    g_settings_bind (self->dock_settings, "dock-fixed",
                    self->dock_autohide_row, "active",
                    G_SETTINGS_BIND_INVERT_BOOLEAN |
                    G_SETTINGS_BIND_NO_SENSITIVITY);
  }
}