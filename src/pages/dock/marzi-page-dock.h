/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_DOCK (marzi_page_dock_get_type ())

G_DECLARE_FINAL_TYPE (MarziPageDock, marzi_page_dock, MARZI, PAGE_DOCK, AdwBin)

G_END_DECLS
