/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-welcome.h"

#include "config.h"
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include <gdk/gdktexture.h>


struct _MarziPageWelcome
{
  AdwBin               parent_instance;

  AdwStatusPage       *session_page;
};

G_DEFINE_TYPE (MarziPageWelcome, marzi_page_welcome, ADW_TYPE_BIN)

static void
update_welcome_page(MarziPageWelcome *self)
{
  const gchar *session_list;
  g_auto(GStrv) session = NULL;
  GdkPaintable *paintable = NULL;

  session_list = g_getenv("XDG_CURRENT_DESKTOP");
  if (session_list != NULL)
    session = g_strsplit(session_list, ":", -1);

  if (session != NULL) {
    if (g_strcmp0(session[0], "ubuntu") == 0)
    {
      paintable = GDK_PAINTABLE(gdk_texture_new_from_resource("/org/zarya/marzi/pages/welcome/assets/ubuntu.svg"));
    } 
    else if (g_strcmp0(session[0], "GNOME") == 0)
    {
      paintable = GDK_PAINTABLE(gdk_texture_new_from_resource("/org/zarya/marzi/pages/welcome/assets/gnome.svg"));
    }
    else if (g_strcmp0(session[0], "zarya") == 0)
    {
      paintable = GDK_PAINTABLE(gdk_texture_new_from_resource("/org/zarya/marzi/pages/welcome/assets/zarya.svg"));
    }

    if (paintable != NULL) {
      adw_status_page_set_paintable(self->session_page, paintable);
    }
  }
}

static void
marzi_page_welcome_class_init (MarziPageWelcomeClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/welcome/marzi-page-welcome.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPageWelcome, session_page);
}

static void
marzi_page_welcome_init (MarziPageWelcome *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
  update_welcome_page(self);
}