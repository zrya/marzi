/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_WELCOME (marzi_page_welcome_get_type ())

G_DECLARE_FINAL_TYPE (MarziPageWelcome, marzi_page_welcome, MARZI, PAGE_WELCOME, AdwBin)

G_END_DECLS
