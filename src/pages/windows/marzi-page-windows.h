/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_WINDOWS (marzi_page_windows_get_type ())

G_DECLARE_FINAL_TYPE (MarziPageWindows, marzi_page_windows, MARZI, PAGE_WINDOWS, AdwBin)

G_END_DECLS
