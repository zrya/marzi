/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-windows.h"

#include "config.h"
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>

#define WM_BUTTON_LAYOUT "button-layout"

#define DEFAULT_BUTTON_LEFT_LAYOUT "close:appmenu"
#define MAXIMIZE_BUTTON_LEFT_LAYOUT "close,maximize:appmenu"
#define MINIMIZE_BUTTON_LEFT_LAYOUT "close,minimize:appmenu"
#define EXTENDED_BUTTON_LEFT_LAYOUT "close,minimize,maximize:appmenu"

#define DEFAULT_BUTTON_RIGHT_LAYOUT "appmenu:close"
#define MAXIMIZE_BUTTON_RIGHT_LAYOUT "appmenu:maximize,close"
#define MINIMIZE_BUTTON_RIGHT_LAYOUT "appmenu:minimize,close"
#define EXTENDED_BUTTON_RIGHT_LAYOUT "appmenu:minimize,maximize,close"


struct _MarziPageWindows
{
  AdwBin            parent_instance;

  GSettings        *wm_settings;

  GtkCheckButton   *left_placement;
  GtkPicture       *left_picture;
  GtkCheckButton   *right_placement;
  GtkPicture       *right_picture;

  AdwSwitchRow     *minimize_row;
  AdwSwitchRow     *maximize_row;
};

G_DEFINE_TYPE (MarziPageWindows, marzi_page_windows, ADW_TYPE_BIN)

static void
on_option_click_released_cb (GtkGestureClick *self,
                             gint             n_press,
                             gdouble          x,
                             gdouble          y,
                             GtkCheckButton  *check_button)
{
  gtk_widget_activate (GTK_WIDGET (check_button));
}

static void
marzi_page_windows_dispose (GObject *object)
{
  MarziPageWindows *self = (MarziPageWindows *)object;

  g_clear_object (&self->wm_settings);

  G_OBJECT_CLASS (marzi_page_windows_parent_class)->dispose (object);
}

static void
reload_titlebar_layout(MarziPageWindows *self)
{
  struct
  {
    const gchar *layout;
    gboolean left_active;
    gboolean right_active;
    gboolean minimize_active;
    gboolean maximize_active;
  } layouts[] = {
      {DEFAULT_BUTTON_RIGHT_LAYOUT, FALSE, TRUE, FALSE, FALSE},
      {DEFAULT_BUTTON_LEFT_LAYOUT, TRUE, FALSE, FALSE, FALSE},
      {EXTENDED_BUTTON_RIGHT_LAYOUT, FALSE, TRUE, TRUE, TRUE},
      {EXTENDED_BUTTON_LEFT_LAYOUT, TRUE, FALSE, TRUE, TRUE},
      {MINIMIZE_BUTTON_RIGHT_LAYOUT, FALSE, TRUE, TRUE, FALSE},
      {MINIMIZE_BUTTON_LEFT_LAYOUT, TRUE, FALSE, TRUE, FALSE},
      {MAXIMIZE_BUTTON_RIGHT_LAYOUT, FALSE, TRUE, FALSE, TRUE},
      {MAXIMIZE_BUTTON_LEFT_LAYOUT, TRUE, FALSE, FALSE, TRUE},
  };

  gchar *titlebar_buttons = g_settings_get_string(self->wm_settings, WM_BUTTON_LAYOUT);

  for (int i = 0; i < G_N_ELEMENTS(layouts); i++)
  {
    if (g_ascii_strcasecmp(titlebar_buttons, layouts[i].layout) == 0)
    {
      gtk_check_button_set_active(self->left_placement, layouts[i].left_active);
      gtk_check_button_set_active(self->right_placement, layouts[i].right_active);
      adw_switch_row_set_active(self->minimize_row, layouts[i].minimize_active);
      adw_switch_row_set_active(self->maximize_row, layouts[i].maximize_active);
      break;
    }
  }
}


static void
set_button_layout(MarziPageWindows *self, const char *left_resource, const char *right_resource, const char *layout)
{
  gtk_picture_set_resource (self->left_picture, left_resource);
  gtk_picture_set_resource (self->right_picture, right_resource);
  g_settings_set_string (self->wm_settings, WM_BUTTON_LAYOUT, layout);
}

static void
on_titlebar_placement_checkbutton_active_cb (MarziPageWindows *self)
{
  gboolean is_left_active = gtk_check_button_get_active (self->left_placement);
  gboolean is_right_active = gtk_check_button_get_active (self->right_placement);

  if (is_left_active && !is_right_active)
  {
    if (adw_switch_row_get_active (self->minimize_row) && adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-all.svg", "/org/zarya/marzi/pages/windows/assets/right-all.svg", EXTENDED_BUTTON_LEFT_LAYOUT);
    }
    else if (!adw_switch_row_get_active (self->minimize_row) && !adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left.svg", "/org/zarya/marzi/pages/windows/assets/right.svg", DEFAULT_BUTTON_LEFT_LAYOUT);
    }
    else if (adw_switch_row_get_active (self->minimize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-min.svg", "/org/zarya/marzi/pages/windows/assets/right-min.svg", MINIMIZE_BUTTON_LEFT_LAYOUT);
    }
    else if (adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-max.svg", "/org/zarya/marzi/pages/windows/assets/right-max.svg", MAXIMIZE_BUTTON_LEFT_LAYOUT);
    }  
  }
  else if (!is_left_active && is_right_active)
  {
    if (adw_switch_row_get_active (self->minimize_row) && adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-all.svg", "/org/zarya/marzi/pages/windows/assets/right-all.svg", EXTENDED_BUTTON_RIGHT_LAYOUT);
    }
    else if (!adw_switch_row_get_active (self->minimize_row) && !adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left.svg", "/org/zarya/marzi/pages/windows/assets/right.svg", DEFAULT_BUTTON_RIGHT_LAYOUT);
    }
    else if (adw_switch_row_get_active (self->minimize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-min.svg", "/org/zarya/marzi/pages/windows/assets/right-min.svg", MINIMIZE_BUTTON_RIGHT_LAYOUT);
    }
    else if (adw_switch_row_get_active (self->maximize_row))
    {
      set_button_layout(self, "/org/zarya/marzi/pages/windows/assets/left-max.svg", "/org/zarya/marzi/pages/windows/assets/right-max.svg", MAXIMIZE_BUTTON_RIGHT_LAYOUT);
    }
  }
}

static void
marzi_page_windows_class_init (MarziPageWindowsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_page_windows_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/windows/marzi-page-windows.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, left_placement);
  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, left_picture);
  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, right_placement);
  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, right_picture);
  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, minimize_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageWindows, maximize_row);

  gtk_widget_class_bind_template_callback (widget_class, on_titlebar_placement_checkbutton_active_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_option_click_released_cb);
}

static void
marzi_page_windows_init (MarziPageWindows *self)
{  
  gtk_widget_init_template (GTK_WIDGET (self));

  self->wm_settings = g_settings_new ("org.gnome.desktop.wm.preferences");
  /* Wm settings */
  reload_titlebar_layout (self);

  g_signal_connect_object (self->wm_settings,
                           "changed::" WM_BUTTON_LAYOUT,
                           G_CALLBACK (reload_titlebar_layout),
                           self,
                           G_CONNECT_SWAPPED);
}