/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_DONE (marzi_page_done_get_type ())

G_DECLARE_FINAL_TYPE (MarziPageDone, marzi_page_done, MARZI, PAGE_DONE, AdwBin)

G_END_DECLS
