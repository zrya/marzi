/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-done.h"

#include "config.h"
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include <gdk/gdktexture.h>


struct _MarziPageDone
{
  AdwBin               parent_instance;
};

G_DEFINE_TYPE (MarziPageDone, marzi_page_done, ADW_TYPE_BIN)


static void
marzi_page_done_class_init (MarziPageDoneClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/done/marzi-page-done.ui");
}

static void
marzi_page_done_init (MarziPageDone *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}