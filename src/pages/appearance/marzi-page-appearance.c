/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-appearance.h"
#include "marzi-dialog-schedule.h"
#include "marzi-dialog-night-light.h"
#include "background/cc-background-item.h"
#include "background/cc-background-preview.h"
#include "background/cc-background-xml.h"
#include "ubuntu/cc-ubuntu-colors-row.h"

#include "config.h"
#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include <gdesktop-enums.h>

#define WP_PATH_ID "org.gnome.desktop.background"
#define WP_LOCK_PATH_ID "org.gnome.desktop.screensaver"
#define WP_URI_KEY "picture-uri"
#define WP_URI_DARK_KEY "picture-uri-dark"
#define WP_OPTIONS_KEY "picture-options"
#define WP_SHADING_KEY "color-shading-type"
#define WP_PCOLOR_KEY "primary-color"
#define WP_SCOLOR_KEY "secondary-color"

#define INTERFACE_PATH_ID "org.gnome.desktop.interface"
#define INTERFACE_COLOR_SCHEME_KEY "color-scheme"
#define DISPLAY_SCHEMA   "org.gnome.settings-daemon.plugins.color"

struct _MarziPageAppearance
{
  AdwBin               parent_instance;

  CcBackgroundItem    *current_background;

  GtkToggleButton     *default_toggle;
  GtkToggleButton     *dark_toggle;
  CcBackgroundPreview *default_preview;
  CcBackgroundPreview *dark_preview;
  AdwSwitchRow        *night_light_row;

  GtkButton           *night_light_configure_button;

  GSettings           *settings;
  GSettings           *interface_settings;
  GSettings           *settings_display;

  CcUbuntuColorsRow *ubuntu_colors;
};

G_DEFINE_TYPE (MarziPageAppearance, marzi_page_appearance, ADW_TYPE_BIN)

static void
run_window (MarziPageAppearance *self, AdwWindow *window)
{
  GtkNative *native = gtk_widget_get_native (GTK_WIDGET (self));

  gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (native));
  gtk_window_present (GTK_WINDOW (window));
}

static void
activate_row (MarziPageAppearance *self, GtkButton *button)
{
  if (button == self->night_light_configure_button)
  {
    run_window (self, ADW_WINDOW (marzi_dialog_night_light_new ()));
  }
}

static void
reload_color_scheme_toggles (MarziPageAppearance *self)
{
  GDesktopColorScheme scheme;

  scheme = g_settings_get_enum (self->interface_settings, INTERFACE_COLOR_SCHEME_KEY);

  if (scheme == G_DESKTOP_COLOR_SCHEME_DEFAULT)
    {
      gtk_toggle_button_set_active (self->default_toggle, TRUE);
    }
  else if (scheme == G_DESKTOP_COLOR_SCHEME_PREFER_DARK)
    {
      gtk_toggle_button_set_active (self->dark_toggle, TRUE);
    }
  else
    {
      gtk_toggle_button_set_active (self->default_toggle, FALSE);
      gtk_toggle_button_set_active (self->dark_toggle, FALSE);
    }
  cc_ubuntu_colors_row_set_color_scheme (self->ubuntu_colors, scheme);
}

static void
set_color_scheme (MarziPageAppearance   *self,
                  GDesktopColorScheme  color_scheme)
{
  GDesktopColorScheme scheme;

  scheme = g_settings_get_enum (self->interface_settings,
                                INTERFACE_COLOR_SCHEME_KEY);
  if (color_scheme == scheme)
    return;

  g_settings_set_enum (self->interface_settings,
                       INTERFACE_COLOR_SCHEME_KEY,
                       color_scheme);
}

/* Color schemes */

static void
on_color_scheme_toggle_active_cb (MarziPageAppearance *self)
{
  if (gtk_toggle_button_get_active (self->default_toggle))
    set_color_scheme (self, G_DESKTOP_COLOR_SCHEME_DEFAULT);
  else if (gtk_toggle_button_get_active (self->dark_toggle))
    set_color_scheme (self, G_DESKTOP_COLOR_SCHEME_PREFER_DARK);
}

/* Background */

static void
update_preview (MarziPageAppearance *self)
{
  CcBackgroundItem *current_background;

  current_background = self->current_background;
  cc_background_preview_set_item (self->default_preview, current_background);
  cc_background_preview_set_item (self->dark_preview, current_background);
}

static gchar *
get_save_path (void)
{
  return g_build_filename (g_get_user_config_dir (),
                           "gnome-control-center",
                           "backgrounds",
                           "last-edited.xml",
                           NULL);
}

static void
reload_current_bg (MarziPageAppearance *self)
{
  g_autoptr(CcBackgroundItem) saved = NULL;
  CcBackgroundItem *configured;
  GSettings *settings = NULL;
  g_autofree gchar *uri = NULL;
  g_autofree gchar *dark_uri = NULL;
  g_autofree gchar *pcolor = NULL;
  g_autofree gchar *scolor = NULL;

  /* Load the saved configuration */
  uri = get_save_path ();
  saved = cc_background_xml_get_item (uri);

  /* initalise the current background information from settings */
  settings = self->settings;
  uri = g_settings_get_string (settings, WP_URI_KEY);
  if (uri && *uri == '\0')
    g_clear_pointer (&uri, g_free);


  configured = cc_background_item_new (uri);

  dark_uri = g_settings_get_string (settings, WP_URI_DARK_KEY);
  pcolor = g_settings_get_string (settings, WP_PCOLOR_KEY);
  scolor = g_settings_get_string (settings, WP_SCOLOR_KEY);
  g_object_set (G_OBJECT (configured),
                "name", _("Current background"),
                "uri-dark", dark_uri,
                "placement", g_settings_get_enum (settings, WP_OPTIONS_KEY),
                "shading", g_settings_get_enum (settings, WP_SHADING_KEY),
                "primary-color", pcolor,
                "secondary-color", scolor,
                NULL);

  if (saved != NULL && cc_background_item_compare (saved, configured))
    {
      CcBackgroundItemFlags flags;
      flags = cc_background_item_get_flags (saved);
      /* Special case for colours */
      if (cc_background_item_get_placement (saved) == G_DESKTOP_BACKGROUND_STYLE_NONE)
        flags &=~ (CC_BACKGROUND_ITEM_HAS_PCOLOR | CC_BACKGROUND_ITEM_HAS_SCOLOR);
      g_object_set (G_OBJECT (configured),
		    "name", cc_background_item_get_name (saved),
		    "flags", flags,
		    "source-url", cc_background_item_get_source_url (saved),
		    "source-xml", cc_background_item_get_source_xml (saved),
		    NULL);
    }

  g_clear_object (&self->current_background);
  self->current_background = configured;
  cc_background_item_load (configured, NULL);
}

static void
marzi_page_appearance_dispose (GObject *object)
{
  MarziPageAppearance *self = (MarziPageAppearance *)object;

  g_clear_object (&self->settings);
  g_clear_object (&self->interface_settings);
  g_clear_object (&self->settings_display);

  G_OBJECT_CLASS (marzi_page_appearance_parent_class)->dispose (object);
}

static void
marzi_page_appearance_class_init (MarziPageAppearanceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure (CC_TYPE_BACKGROUND_PREVIEW);
  g_type_ensure (CC_TYPE_UBUNTU_COLORS_ROW);

  object_class->dispose = marzi_page_appearance_dispose;
  
  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/appearance/marzi-page-appearance.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, default_toggle);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, dark_toggle);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, default_preview);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, dark_preview);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, night_light_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, night_light_configure_button);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAppearance, ubuntu_colors);

  gtk_widget_class_bind_template_callback (widget_class, activate_row);
  gtk_widget_class_bind_template_callback (widget_class, on_color_scheme_toggle_active_cb);
}

static void
on_settings_changed (MarziPageAppearance *self)
{
  reload_current_bg (self);
  update_preview (self);
}

static void
marzi_page_appearance_init (MarziPageAppearance *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new (WP_PATH_ID);
  g_settings_delay (self->settings);

  self->interface_settings = g_settings_new (INTERFACE_PATH_ID);
  self->settings_display = g_settings_new (DISPLAY_SCHEMA);

  /* Load the background */
  reload_current_bg (self);
  update_preview (self);

  /* Background settings */
  g_signal_connect_object (self->settings, "changed", G_CALLBACK (on_settings_changed), self, G_CONNECT_SWAPPED);

  /* Interface settings */
  reload_color_scheme_toggles (self);

  g_signal_connect_object (self->interface_settings,
                           "changed::" INTERFACE_COLOR_SCHEME_KEY,
                           G_CALLBACK (reload_color_scheme_toggles),
                           self,
                           G_CONNECT_SWAPPED);

  g_settings_bind (self->settings_display, "night-light-enabled",
                   self->night_light_row, "active",
                   G_SETTINGS_BIND_DEFAULT);
}