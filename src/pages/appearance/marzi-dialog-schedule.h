/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_APPEARANCE (marzi_page_appearance_get_type ())

G_DECLARE_FINAL_TYPE (MarziDialogSchedule, marzi_dialog_schedule, MARZI, DIALOG_SCHEDULE, AdwWindow)

MarziDialogSchedule *marzi_dialog_schedule_new (void);

G_END_DECLS