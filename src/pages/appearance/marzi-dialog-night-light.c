/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "/usr/include/gsettings-desktop-schemas/gdesktop-enums.h"
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <math.h>

#include "marzi-dialog-night-light.h"

struct _MarziDialogNightLight
{
    AdwWindow           parent;

    GtkCheckButton      *automatic_radio;
    GtkCheckButton      *manual_radio;
    GtkWidget           *manual_combo_row;

    GtkWidget           *from_spinbuttons_box;
    GtkWidget           *spinbutton_from_hours;
    GtkWidget           *spinbutton_from_minutes;
    GtkButton           *button_from_am;
    GtkButton           *button_from_pm;
    GtkStack            *stack_from;

    GtkWidget           *to_spinbuttons_box;
    GtkWidget           *spinbutton_to_hours;
    GtkWidget           *spinbutton_to_minutes;
    GtkButton           *button_to_am;
    GtkButton           *button_to_pm;
    GtkStack            *stack_to;

    GtkAdjustment       *adjustment_from_hours;
    GtkAdjustment       *adjustment_from_minutes;
    GtkAdjustment       *adjustment_to_hours;
    GtkAdjustment       *adjustment_to_minutes;

    GSettings           *settings_display;
    GSettings           *settings_clock;
    gboolean             ignore_value_changed;
    GDesktopClockFormat  clock_format;
};

G_DEFINE_TYPE (MarziDialogNightLight, marzi_dialog_night_light, ADW_TYPE_WINDOW)

#define CLOCK_SCHEMA     "org.gnome.desktop.interface"
#define CLOCK_FORMAT_KEY "clock-format"
#define DISPLAY_SCHEMA   "org.gnome.settings-daemon.plugins.color"

static void
dialog_adjustments_set_frac_hours (MarziDialogNightLight *self,
                                   gdouble              value,
                                   GtkAdjustment       *adj_hours,
                                   GtkAdjustment       *adj_mins,
                                   GtkStack            *stack,
                                   GtkButton           *button_am,
                                   GtkButton           *button_pm)
{
  gdouble hours;
  gdouble mins = 0.f;
  gboolean is_pm = FALSE;
  gboolean is_24h;

  /* display the right thing for AM/PM */
  is_24h = self->clock_format == G_DESKTOP_CLOCK_FORMAT_24H;
  mins = modf (value, &hours) * 60.f;
  if (!is_24h)
    {
      if (hours > 12)
        {
          hours -= 12;
          is_pm = TRUE;
        }
      else if (hours < 1.0)
        {
          hours += 12;
          is_pm = FALSE;
        }
      else if (hours == 12.f)
        {
          is_pm = TRUE;
        }
    }

  g_debug ("setting adjustment %.3f to %.0f:%02.0f", value, hours, mins);

  self->ignore_value_changed = TRUE;
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adj_hours), hours);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (adj_mins), mins);
  self->ignore_value_changed = FALSE;

  gtk_widget_set_visible (GTK_WIDGET (stack), !is_24h);
  gtk_stack_set_visible_child (stack, is_pm ? GTK_WIDGET (button_pm) : GTK_WIDGET (button_am));
}

static void
dialog_update_state (MarziDialogNightLight *self)
{
    gboolean automatic;
    gdouble value = 0.f;

    automatic = g_settings_get_boolean (self->settings_display, "night-light-schedule-automatic");

    if (!automatic)
    {
        gtk_widget_set_sensitive (self->manual_combo_row, TRUE);
        value = g_settings_get_double (self->settings_display, "night-light-schedule-from");
        value = fmod (value, 24.f);

        dialog_adjustments_set_frac_hours (self, value,
                                           self->adjustment_from_hours,
                                           self->adjustment_from_minutes,
                                           self->stack_from,
                                           self->button_from_am,
                                           self->button_from_pm);

        value = g_settings_get_double (self->settings_display, "night-light-schedule-to");
        value = fmod (value, 24.f);

        dialog_adjustments_set_frac_hours (self, value,
                                           self->adjustment_to_hours,
                                           self->adjustment_to_minutes,
                                           self->stack_to,
                                           self->button_to_am,
                                           self->button_to_pm);
    }
    else
      gtk_widget_set_sensitive (self->manual_combo_row, FALSE);
}

static gdouble
dialog_adjustments_get_frac_hours (MarziDialogNightLight *self,
                                   GtkAdjustment       *adj_hours,
                                   GtkAdjustment       *adj_mins,
                                   GtkStack            *stack)
{
  gdouble value;

  value = gtk_adjustment_get_value (adj_hours);
  value += gtk_adjustment_get_value (adj_mins) / 60.0f;

  if (g_strcmp0 (gtk_stack_get_visible_child_name (stack), "pm") == 0)
    value += 12.f;

  return value;
}

static void
dialog_time_from_value_changed_cb (GtkAdjustment       *adjustment,
                                   MarziDialogNightLight *self)
{
  gdouble value;

  if (self->ignore_value_changed)
    return;

  value = dialog_adjustments_get_frac_hours (self,
                                             self->adjustment_from_hours,
                                             self->adjustment_from_minutes,
                                             self->stack_from);

  if (value >= 24.f)
    value = fmod (value, 24);

  g_debug ("new value = %.3f", value);

  g_settings_set_double (self->settings_display, "night-light-schedule-from", value);
}

static void
dialog_time_to_value_changed_cb (GtkAdjustment    *adjustment,
                                 MarziDialogNightLight *self)
{
  gdouble value;

  if (self->ignore_value_changed)
    return;

  value = dialog_adjustments_get_frac_hours (self,
                                             self->adjustment_to_hours,
                                             self->adjustment_to_minutes,
                                             self->stack_to);
  if (value >= 24.f)
    value = fmod (value, 24);

  g_debug ("new value = %.3f", value);

  g_settings_set_double (self->settings_display, "night-light-schedule-to", value);
}

static gboolean
dialog_format_minutes_combobox (GtkSpinButton    *spin,
                                MarziDialogNightLight *self)
{
  GtkAdjustment *adjustment;
  g_autofree gchar *text = NULL;
  adjustment = gtk_spin_button_get_adjustment (spin);
  text = g_strdup_printf ("%02.0f", gtk_adjustment_get_value (adjustment));
  gtk_editable_set_text (GTK_EDITABLE (spin), text);
  return TRUE;
}

static gboolean
dialog_format_hours_combobox (GtkSpinButton      *spin,
                              MarziDialogNightLight *self)
{
  GtkAdjustment *adjustment;
  g_autofree gchar *text = NULL;
  adjustment = gtk_spin_button_get_adjustment (spin);
  if (self->clock_format == G_DESKTOP_CLOCK_FORMAT_12H)
    text = g_strdup_printf ("%.0f", gtk_adjustment_get_value (adjustment));
  else
    text = g_strdup_printf ("%02.0f", gtk_adjustment_get_value (adjustment));
  gtk_editable_set_text (GTK_EDITABLE (spin), text);
  return TRUE;
}

static void
dialog_update_adjustments (MarziDialogNightLight *self)
{
  /* from */
  if (self->clock_format == G_DESKTOP_CLOCK_FORMAT_24H)
    {
      gtk_adjustment_set_lower (self->adjustment_from_hours, 0);
      gtk_adjustment_set_upper (self->adjustment_from_hours, 23);
    }
  else
    {
      if (gtk_adjustment_get_value (self->adjustment_from_hours) > 12)
          gtk_stack_set_visible_child (self->stack_from, GTK_WIDGET (self->button_from_pm));

      gtk_adjustment_set_lower (self->adjustment_from_hours, 1);
      gtk_adjustment_set_upper (self->adjustment_from_hours, 12);
    }

  /* to */
  if (self->clock_format == G_DESKTOP_CLOCK_FORMAT_24H)
    {
      gtk_adjustment_set_lower (self->adjustment_to_hours, 0);
      gtk_adjustment_set_upper (self->adjustment_to_hours, 23);
    }
  else
    {
      if (gtk_adjustment_get_value (self->adjustment_to_hours) > 12)
          gtk_stack_set_visible_child (self->stack_to, GTK_WIDGET (self->button_to_pm));

      gtk_adjustment_set_lower (self->adjustment_to_hours, 1);
      gtk_adjustment_set_upper (self->adjustment_to_hours, 12);
    }
}

static void
dialog_settings_changed_cb (MarziDialogNightLight *self)
{
  dialog_update_state (self);
}

static void
dialog_clock_settings_changed_cb (MarziDialogNightLight *self)
{
  self->clock_format = g_settings_get_enum (self->settings_clock, CLOCK_FORMAT_KEY);

  /* uncontionally widen this to avoid truncation */
  gtk_adjustment_set_lower (self->adjustment_from_hours, 0);
  gtk_adjustment_set_upper (self->adjustment_from_hours, 23);
  gtk_adjustment_set_lower (self->adjustment_to_hours, 0);
  gtk_adjustment_set_upper (self->adjustment_to_hours, 23);

  /* update spinbuttons */
  gtk_spin_button_update (GTK_SPIN_BUTTON (self->spinbutton_from_hours));
  gtk_spin_button_update (GTK_SPIN_BUTTON (self->spinbutton_to_hours));

  /* update UI */
  dialog_update_state (self);
  dialog_update_adjustments (self);
}

static void
dialog_am_pm_from_button_clicked_cb (GtkButton          *button,
                                     MarziDialogNightLight *self)
{
  gdouble value;
  value = g_settings_get_double (self->settings_display, "night-light-schedule-from");
  if (value > 12.f)
    value -= 12.f;
  else
    value += 12.f;
  if (value >= 24.f)
    value = fmod (value, 24);
  g_settings_set_double (self->settings_display, "night-light-schedule-from", value);
  g_debug ("new value = %.3f", value);
}

static void
dialog_am_pm_to_button_clicked_cb (GtkButton           *button,
                                   MarziDialogNightLight *self)
{
  gdouble value;
  value = g_settings_get_double (self->settings_display, "night-light-schedule-to");
  if (value > 12.f)
    value -= 12.f;
  else
    value += 12.f;
  if (value >= 24.f)
    value = fmod (value, 24);
  g_settings_set_double (self->settings_display, "night-light-schedule-to", value);
  g_debug ("new value = %.3f", value);
}

static void
marzi_dialog_night_light_dispose (GObject *object)
{
  MarziDialogNightLight *self = (MarziDialogNightLight *)object;

  g_clear_object (&self->settings_display);

  G_OBJECT_CLASS (marzi_dialog_night_light_parent_class)->dispose (object);
}

static void
marzi_dialog_night_light_class_init (MarziDialogNightLightClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_dialog_night_light_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/appearance/marzi-dialog-night-light.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, automatic_radio);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, manual_radio);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, manual_combo_row);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, from_spinbuttons_box);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, spinbutton_from_hours);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, spinbutton_from_minutes);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, to_spinbuttons_box);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, spinbutton_to_hours);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, spinbutton_to_minutes);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, stack_from);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, stack_to);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, button_from_am);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, button_from_pm);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, button_to_am);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, button_to_pm);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, adjustment_from_hours);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, adjustment_from_minutes);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, adjustment_to_hours);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogNightLight, adjustment_to_minutes);

  gtk_widget_class_bind_template_callback (widget_class, dialog_am_pm_from_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, dialog_am_pm_to_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, dialog_format_hours_combobox);
  gtk_widget_class_bind_template_callback (widget_class, dialog_format_minutes_combobox);
  gtk_widget_class_bind_template_callback (widget_class, dialog_time_from_value_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, dialog_time_to_value_changed_cb);
}

static void
marzi_dialog_night_light_init (MarziDialogNightLight *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings_display = g_settings_new (DISPLAY_SCHEMA);

  g_signal_connect_object (self->settings_display, "changed", G_CALLBACK (dialog_settings_changed_cb), self, G_CONNECT_SWAPPED);

  g_settings_bind_writable (self->settings_display, "night-light-schedule-from",
                            self->spinbutton_from_hours, "sensitive",
                            FALSE);
  g_settings_bind_writable (self->settings_display, "night-light-schedule-from",
                            self->spinbutton_from_minutes, "sensitive",
                            FALSE);
  g_settings_bind_writable (self->settings_display, "night-light-schedule-to",
                            self->spinbutton_to_minutes, "sensitive",
                            FALSE);
  g_settings_bind_writable (self->settings_display, "night-light-schedule-to",
                            self->spinbutton_to_minutes, "sensitive",
                            FALSE);

  if (g_settings_get_boolean (self->settings_display, "night-light-schedule-automatic"))
    gtk_check_button_set_active (self->automatic_radio, TRUE);
  else
    gtk_check_button_set_active (self->manual_radio, TRUE);

  g_settings_bind (self->settings_display, "night-light-schedule-automatic",
                   self->automatic_radio, "active",
                   G_SETTINGS_BIND_DEFAULT);

  /* clock settings_display */
  self->settings_clock = g_settings_new (CLOCK_SCHEMA);
  self->clock_format = g_settings_get_enum (self->settings_clock, CLOCK_FORMAT_KEY);
  dialog_update_adjustments (self);
  g_signal_connect_object (self->settings_clock,
                           "changed::" CLOCK_FORMAT_KEY,
                           G_CALLBACK (dialog_clock_settings_changed_cb),
                           self, G_CONNECT_SWAPPED);

  if (gtk_widget_get_default_direction () == GTK_TEXT_DIR_RTL)
    {
      gtk_widget_set_direction (self->to_spinbuttons_box, GTK_TEXT_DIR_LTR);
      gtk_widget_set_direction (self->from_spinbuttons_box, GTK_TEXT_DIR_LTR);
    }

  dialog_update_state (self);
}

MarziDialogNightLight *
marzi_dialog_night_light_new (void)
{
  return g_object_new (marzi_dialog_night_light_get_type (), NULL);
}
