/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-page-accessibility.h"
#include "marzi-dialog-seeing.h"
#include "marzi-dialog-hearing.h"
#include "marzi-dialog-typing.h"
#include "marzi-dialog-pointing.h"

#include "config.h"

#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include "/usr/include/gsettings-desktop-schemas/gdesktop-enums.h"

#define A11Y_SETTINGS                "org.gnome.desktop.a11y"
#define KEY_ALWAYS_SHOW_STATUS       "always-show-universal-access-status"

struct _MarziPageAccessibility
{
  AdwBin             parent_instance;

  GtkSwitch         *show_ua_menu_switch;
  AdwActionRow      *seeing_row;
  AdwActionRow      *hearing_row;
  AdwActionRow      *typing_row;
  AdwActionRow      *pointing_row;

  GSettings         *a11y_settings;
};

G_DEFINE_TYPE (MarziPageAccessibility, marzi_page_accessibility, ADW_TYPE_BIN)

static void
run_window (MarziPageAccessibility *self, AdwWindow *window)
{
  GtkNative *native = gtk_widget_get_native (GTK_WIDGET (self));

  gtk_window_set_transient_for (GTK_WINDOW (window), GTK_WINDOW (native));
  gtk_window_present (GTK_WINDOW (window));
}

static void
activate_row (MarziPageAccessibility *self, AdwActionRow *row)
{
  if (row == self->seeing_row)
    {
      run_window (self, ADW_WINDOW (marzi_dialog_seeing_new ()));
    }
  else if (row == self->hearing_row)
    {
      run_window (self, ADW_WINDOW (marzi_dialog_hearing_new ()));
    }
  else if (row == self->typing_row)
    {
      run_window (self, ADW_WINDOW (marzi_dialog_typing_new ()));
    }
  else if (row == self->pointing_row)
    {
      run_window (self, ADW_WINDOW (marzi_dialog_pointing_new ()));
    }
}

static void
marzi_page_accessibility_dispose (GObject *object)
{
  MarziPageAccessibility *self = (MarziPageAccessibility *)object;

  g_clear_object (&self->a11y_settings);

  G_OBJECT_CLASS (marzi_page_accessibility_parent_class)->dispose (object);
}

static void
marzi_page_accessibility_class_init (MarziPageAccessibilityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_page_accessibility_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/accessibility/marzi-page-accessibility.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziPageAccessibility, seeing_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAccessibility, hearing_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAccessibility, typing_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAccessibility, pointing_row);
  gtk_widget_class_bind_template_child (widget_class, MarziPageAccessibility, show_ua_menu_switch);

  gtk_widget_class_bind_template_callback (widget_class, activate_row);
}

static void
marzi_page_accessibility_init (MarziPageAccessibility *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->a11y_settings = g_settings_new (A11Y_SETTINGS);
  g_settings_bind (self->a11y_settings, KEY_ALWAYS_SHOW_STATUS,
                   self->show_ua_menu_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}