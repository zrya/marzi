/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-dialog-typing.h"

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>

/* application settings */
#define APPLICATION_SETTINGS         "org.gnome.desktop.a11y.applications"
#define KEY_SCREEN_KEYBOARD_ENABLED  "screen-keyboard-enabled"

/* keyboard settings */
#define KEYBOARD_SETTINGS            "org.gnome.desktop.a11y.keyboard"
#define KEY_BOUNCEKEYS_ENABLED       "bouncekeys-enable"
#define KEY_SLOWKEYS_ENABLED         "slowkeys-enable"
#define KEY_STICKYKEYS_ENABLED       "stickykeys-enable"

struct _MarziDialogTyping
{
  AdwWindow           parent;

  GtkSwitch          *screen_kb_switch;
  GtkSwitch          *sticky_keys_switch;
  GtkSwitch          *slow_keys_switch;
  GtkSwitch          *bounce_keys_switch;

  GSettings          *application_settings;
  GSettings          *kb_settings;
};

G_DEFINE_TYPE (MarziDialogTyping, marzi_dialog_typing, ADW_TYPE_WINDOW)


static void
marzi_dialog_typing_dispose (GObject *object)
{
  MarziDialogTyping *self = (MarziDialogTyping *)object;

  g_clear_object (&self->application_settings);
  g_clear_object (&self->kb_settings);

  G_OBJECT_CLASS (marzi_dialog_typing_parent_class)->dispose (object);
}

static void
marzi_dialog_typing_class_init (MarziDialogTypingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_dialog_typing_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/accessibility/marzi-dialog-typing.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziDialogTyping, screen_kb_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogTyping, sticky_keys_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogTyping, slow_keys_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogTyping, bounce_keys_switch);
}

static void
marzi_dialog_typing_init (MarziDialogTyping *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->application_settings = g_settings_new (APPLICATION_SETTINGS);
  self->kb_settings = g_settings_new (KEYBOARD_SETTINGS);

  g_settings_bind (self->application_settings, KEY_SCREEN_KEYBOARD_ENABLED,
                   self->screen_kb_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  /* Sticky keys */
  g_settings_bind (self->kb_settings, KEY_STICKYKEYS_ENABLED,
                   self->sticky_keys_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  /* Slow Keys */
  g_settings_bind (self->kb_settings, KEY_SLOWKEYS_ENABLED,
                   self->slow_keys_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  /* Bounce Keys */
  g_settings_bind (self->kb_settings, KEY_BOUNCEKEYS_ENABLED,
                   self->bounce_keys_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

MarziDialogTyping *
marzi_dialog_typing_new (void)
{
  return g_object_new (marzi_dialog_typing_get_type (), NULL);
}