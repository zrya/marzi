/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-dialog-hearing.h"

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include "/usr/include/gsettings-desktop-schemas/gdesktop-enums.h"

/* sound settings */
#define SOUND_SETTINGS               "org.gnome.desktop.sound"
#define KEY_SOUND_OVERAMPLIFY        "allow-volume-above-100-percent"

/* wm settings */
#define WM_SETTINGS                  "org.gnome.desktop.wm.preferences"
#define KEY_VISUAL_BELL              "visual-bell"
#define KEY_VISUAL_BELL_TYPE         "visual-bell-type"

struct _MarziDialogHearing
{
  AdwWindow           parent;

  GtkSwitch          *overamplification_switch;
  GtkSwitch          *visual_alerts_switch;
  AdwComboRow        *flash_type_row;
  AdwActionRow       *test_flash_row;

  GSettings          *sound_settings;
  GSettings          *wm_settings;
};

G_DEFINE_TYPE (MarziDialogHearing, marzi_dialog_hearing, ADW_TYPE_WINDOW)

static void
marzi_dialog_hearing_flash_type_changed_cb (MarziDialogHearing *self)
{
  GDesktopVisualBellType type;

  type = g_settings_get_enum (self->wm_settings, KEY_VISUAL_BELL_TYPE);
  adw_combo_row_set_selected (self->flash_type_row, type);
}

static void
marzi_dialog_hearing_flash_type_row_changed_cb (MarziDialogHearing *self)
{
  guint selected_index;

  selected_index = adw_combo_row_get_selected (self->flash_type_row);
  g_settings_set_enum (self->wm_settings, KEY_VISUAL_BELL_TYPE, selected_index);
}

static void
marzi_dialog_hearing_test_flash_clicked_cb (MarziDialogHearing *self)
{
  GdkSurface *surface;
  GtkNative *native;

  native = gtk_widget_get_native (GTK_WIDGET (self));
  surface = gtk_native_get_surface (native);

  gdk_surface_beep (surface);
}

static void
marzi_dialog_hearing_dispose (GObject *object)
{
  MarziDialogHearing *self = (MarziDialogHearing *)object;

  g_clear_object (&self->sound_settings);
  g_clear_object (&self->wm_settings);

  G_OBJECT_CLASS (marzi_dialog_hearing_parent_class)->dispose (object);
}

static void
marzi_dialog_hearing_class_init (MarziDialogHearingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_dialog_hearing_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/accessibility/marzi-dialog-hearing.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziDialogHearing, overamplification_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogHearing, visual_alerts_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogHearing, flash_type_row);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogHearing, test_flash_row);

  gtk_widget_class_bind_template_callback (widget_class, marzi_dialog_hearing_flash_type_row_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, marzi_dialog_hearing_test_flash_clicked_cb);
}

static void
marzi_dialog_hearing_init (MarziDialogHearing *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->sound_settings = g_settings_new (SOUND_SETTINGS);
  self->wm_settings = g_settings_new (WM_SETTINGS);

  g_settings_bind (self->sound_settings, KEY_SOUND_OVERAMPLIFY,
                   self->overamplification_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->wm_settings, KEY_VISUAL_BELL,
                   self->visual_alerts_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_signal_connect_object (self->wm_settings, "changed::" KEY_VISUAL_BELL_TYPE,
                           G_CALLBACK (marzi_dialog_hearing_flash_type_changed_cb), self, G_CONNECT_SWAPPED);
}

MarziDialogHearing *
marzi_dialog_hearing_new (void)
{
  return g_object_new (marzi_dialog_hearing_get_type (),  NULL);
}