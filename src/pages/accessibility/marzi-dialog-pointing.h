/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_ACCESSIBILITY (marzi_page_accessibility_get_type ())

G_DECLARE_FINAL_TYPE (MarziDialogPointing, marzi_dialog_pointing, MARZI, DIALOG_POINTING, AdwWindow)

MarziDialogPointing *marzi_dialog_pointing_new (void);

G_END_DECLS
