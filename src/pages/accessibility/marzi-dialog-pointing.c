/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-dialog-pointing.h"

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>

/* interface settings */
#define INTERFACE_SETTINGS           "org.gnome.desktop.interface"
#define KEY_LOCATE_POINTER           "locate-pointer"

/* keyboard settings */
#define KEYBOARD_SETTINGS            "org.gnome.desktop.a11y.keyboard"
#define KEY_MOUSEKEYS_ENABLED        "mousekeys-enable"

struct _MarziDialogPointing
{
  AdwWindow           parent;

  GtkSwitch          *mouse_keys_switch;
  GtkSwitch          *locate_pointer_switch;
  
  GSettings          *interface_settings;
  GSettings          *kb_settings;
};

G_DEFINE_TYPE (MarziDialogPointing, marzi_dialog_pointing, ADW_TYPE_WINDOW)


static void
marzi_dialog_pointing_dispose (GObject *object)
{
  MarziDialogPointing *self = (MarziDialogPointing *)object;

  g_clear_object (&self->interface_settings);
  g_clear_object (&self->kb_settings);

  G_OBJECT_CLASS (marzi_dialog_pointing_parent_class)->dispose (object);
}

static void
marzi_dialog_pointing_class_init (MarziDialogPointingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_dialog_pointing_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/accessibility/marzi-dialog-pointing.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziDialogPointing, mouse_keys_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogPointing, locate_pointer_switch);
}

static void
marzi_dialog_pointing_init (MarziDialogPointing *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->interface_settings = g_settings_new (INTERFACE_SETTINGS);
  self->kb_settings = g_settings_new (KEYBOARD_SETTINGS);

  g_settings_bind (self->kb_settings, KEY_MOUSEKEYS_ENABLED,
                   self->mouse_keys_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  g_settings_bind (self->interface_settings, KEY_LOCATE_POINTER,
                   self->locate_pointer_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
}

MarziDialogPointing *
marzi_dialog_pointing_new (void)
{
  return g_object_new (marzi_dialog_pointing_get_type (), NULL);
}