/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_PAGE_ACCESSIBILITY (marzi_page_accessibility_get_type ())

G_DECLARE_FINAL_TYPE (MarziDialogTyping, marzi_dialog_typing, MARZI, DIALOG_TYPING, AdwWindow)

MarziDialogTyping *marzi_dialog_typing_new (void);

G_END_DECLS
