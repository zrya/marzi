/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "marzi-dialog-seeing.h"

#include "config.h"

#include <string.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include "/usr/include/gsettings-desktop-schemas/gdesktop-enums.h"

#define DPI_FACTOR_LARGE 1.25
#define DPI_FACTOR_NORMAL 1.0
#define HIGH_CONTRAST_THEME     "HighContrast"

/* a11y interface settings */
#define A11Y_INTERFACE_SETTINGS      "org.gnome.desktop.a11y.interface"
#define KEY_HIGH_CONTRAST            "high-contrast"

/* interface settings */
#define INTERFACE_SETTINGS           "org.gnome.desktop.interface"
#define KEY_TEXT_SCALING_FACTOR      "text-scaling-factor"
#define KEY_GTK_THEME                "gtk-theme"
#define KEY_ICON_THEME               "icon-theme"

/* application settings */
#define APPLICATION_SETTINGS         "org.gnome.desktop.a11y.applications"
#define KEY_SCREEN_KEYBOARD_ENABLED  "screen-keyboard-enabled"
#define KEY_SCREEN_MAGNIFIER_ENABLED "screen-magnifier-enabled"
#define KEY_SCREEN_READER_ENABLED    "screen-reader-enabled"

struct _MarziDialogSeeing
{
  AdwWindow           parent;

  GtkSwitch          *high_contrast_switch;
  GtkSwitch          *large_text_switch;
  GtkSwitch          *screen_reader_switch;

  GSettings          *interface_settings;
  GSettings          *application_settings;
  GSettings          *a11y_interface_settings;

  char               *old_gtk_theme;
  char               *old_icon_theme;

  gboolean            is_self_change;
};

G_DEFINE_TYPE (MarziDialogSeeing, marzi_dialog_seeing, ADW_TYPE_WINDOW)

static gboolean
get_large_text_mapping (GValue   *value,
                        GVariant *variant,
                        gpointer  user_data)
{
  gdouble factor;

  factor = g_variant_get_double (variant);
  g_value_set_boolean (value, factor > DPI_FACTOR_NORMAL);

  return TRUE;
}

static GVariant *
set_large_text_mapping (const GValue       *value,
                        const GVariantType *expected_type,
                        gpointer            user_data)
{
  GSettings *settings = user_data;

  if (g_value_get_boolean (value))
    return g_variant_new_double (DPI_FACTOR_LARGE);

  g_settings_reset (settings, KEY_TEXT_SCALING_FACTOR);

  return NULL;
}

static void
marzi_dialog_seeing_a11y_high_contrast_changed_cb (MarziDialogSeeing *self)
{
  self->is_self_change = TRUE;

  if (g_settings_get_boolean (self->a11y_interface_settings, KEY_HIGH_CONTRAST))
    {
      /* xxx: Should we not set high contrast icons? */
      g_settings_set_string (self->interface_settings, KEY_ICON_THEME, HIGH_CONTRAST_THEME);
    }
  else
    {
      if (self->old_icon_theme && !g_str_equal (self->old_gtk_theme, HIGH_CONTRAST_THEME))
        g_settings_set_string (self->interface_settings, KEY_ICON_THEME, self->old_icon_theme);
      else
        g_settings_reset (self->interface_settings, KEY_ICON_THEME);
    }

  self->is_self_change = FALSE;
}

static void
marzi_dialog_seeing_interface_settings_changed_cb (MarziDialogSeeing *self)
{
  if (self->is_self_change)
    return;

  g_free (self->old_gtk_theme);
  g_free (self->old_icon_theme);

  self->old_gtk_theme = g_settings_get_string (self->interface_settings, KEY_GTK_THEME);
  self->old_icon_theme = g_settings_get_string (self->interface_settings, KEY_ICON_THEME);
}

static void
marzi_dialog_seeing_dispose (GObject *object)
{
  MarziDialogSeeing *self = (MarziDialogSeeing *)object;

  g_clear_object (&self->interface_settings);
  g_clear_object (&self->application_settings);
  g_clear_object (&self->a11y_interface_settings);

  g_clear_pointer (&self->old_gtk_theme, g_free);
  g_clear_pointer (&self->old_icon_theme, g_free);

  G_OBJECT_CLASS (marzi_dialog_seeing_parent_class)->dispose (object);
}

static void
marzi_dialog_seeing_class_init (MarziDialogSeeingClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = marzi_dialog_seeing_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/zarya/marzi/pages/accessibility/marzi-dialog-seeing.ui");

  gtk_widget_class_bind_template_child (widget_class, MarziDialogSeeing, high_contrast_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogSeeing, large_text_switch);
  gtk_widget_class_bind_template_child (widget_class, MarziDialogSeeing, screen_reader_switch); 
}

static void
marzi_dialog_seeing_init (MarziDialogSeeing *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->interface_settings = g_settings_new (INTERFACE_SETTINGS);
  self->application_settings = g_settings_new (APPLICATION_SETTINGS);
  self->a11y_interface_settings = g_settings_new (A11Y_INTERFACE_SETTINGS);

  /* High contrast */
  g_settings_bind (self->a11y_interface_settings, KEY_HIGH_CONTRAST,
                   self->high_contrast_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);
  
  /* Large Text */
  g_settings_bind_with_mapping (self->interface_settings, KEY_TEXT_SCALING_FACTOR,
                                self->large_text_switch,
                                "active", G_SETTINGS_BIND_DEFAULT,
                                get_large_text_mapping,
                                set_large_text_mapping,
                                self->interface_settings,
                                NULL);

  /* Screen Reader */
  g_settings_bind (self->application_settings, KEY_SCREEN_READER_ENABLED,
                   self->screen_reader_switch, "active",
                   G_SETTINGS_BIND_DEFAULT);

  g_signal_connect_object (self->a11y_interface_settings, "changed::" KEY_HIGH_CONTRAST,
                           G_CALLBACK (marzi_dialog_seeing_a11y_high_contrast_changed_cb),
                           self, G_CONNECT_SWAPPED | G_CONNECT_AFTER);

  g_signal_connect_object (self->interface_settings, "changed",
                           G_CALLBACK (marzi_dialog_seeing_interface_settings_changed_cb),
                           self, G_CONNECT_SWAPPED | G_CONNECT_AFTER);

  marzi_dialog_seeing_interface_settings_changed_cb (self);
}

MarziDialogSeeing *
marzi_dialog_seeing_new (void)
{
  return g_object_new (marzi_dialog_seeing_get_type (), NULL);
}