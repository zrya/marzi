/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define MARZI_TYPE_APPLICATION (marzi_application_get_type())

G_DECLARE_FINAL_TYPE (MarziApplication, marzi_application, MARZI, APPLICATION, AdwApplication)

MarziApplication *marzi_application_new (const char        *application_id,
                                         GApplicationFlags  flags);

G_END_DECLS
