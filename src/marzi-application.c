/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include "marzi-application.h"
#include "marzi-window.h"

struct _MarziApplication
{
  AdwApplication parent_instance;
};

G_DEFINE_TYPE (MarziApplication, marzi_application, ADW_TYPE_APPLICATION)

MarziApplication *
marzi_application_new (const char        *application_id,
                       GApplicationFlags  flags)
{
  g_return_val_if_fail (application_id != NULL, NULL);

  return g_object_new (MARZI_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
marzi_application_activate (GApplication *app)
{
  GtkWindow *window;
  const gchar *session_list;
  g_auto(GStrv) session = NULL;
  g_autofree char *os = g_get_os_info (G_OS_INFO_KEY_NAME);

  session_list = g_getenv ("XDG_CURRENT_DESKTOP");
  if (session_list != NULL)
    session = g_strsplit (session_list, ":", -1);

  g_assert (MARZI_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL && session != NULL && g_strv_contains ((const gchar * const *) session, "zarya") && g_str_equal (os, "zarya"))
  {
    window = g_object_new (MARZI_TYPE_WINDOW, "application", app, NULL);
    gtk_window_present (window);
  }
  else
    return;
}

static void
marzi_application_class_init (MarziApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = marzi_application_activate;
}

static void
marzi_application_about_action (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  static const char *developers[] = {"Muqtadir", NULL};
  MarziApplication *self = user_data;
  GtkWindow *window = NULL;

  g_assert (MARZI_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  adw_show_about_window (window,
                         "application-name", "marzi",
                         "application-icon", "org.zarya.marzi",
                         "developer-name", "muqtadir",
                         "version", "44.alpha3",
                         "developers", developers,
                         "copyright", "© 2023 Muqtadir",
                         NULL);
}

static void
marzi_application_quit_action (GSimpleAction *action,
                               GVariant      *parameter,
                               gpointer       user_data)
{
  MarziApplication *self = user_data;

  g_assert (MARZI_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}

static const GActionEntry app_actions[] = {
  { "quit", marzi_application_quit_action },
  { "about", marzi_application_about_action },
};

static void
marzi_application_init (MarziApplication *self)
{
  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   app_actions,
                                   G_N_ELEMENTS (app_actions),
                                   self);
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]) { "<primary>q", NULL });
}

