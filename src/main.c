/*
 * Copyright (C) 2021-2022 Muqtadir
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later
 */

#include "config.h"

#include <glib/gi18n.h>

#include "marzi-application.h"

int
main (int   argc,
      char *argv[])
{
	g_autoptr(MarziApplication) app = NULL;
	int ret;

	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	app = marzi_application_new ("org.zarya.marzi", G_APPLICATION_FLAGS_NONE);
	ret = g_application_run (G_APPLICATION (app), argc, argv);

	return ret;
}
