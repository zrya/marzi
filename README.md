### marzi

`marzi` is an urdu/hindi word and it is loosely synonymous with `choice/wish` and it's main app focus is to guide new-users to configure their first setup.

marzi is written in c,gtk4+libadwaita and kinda re-uses much of the code from `gnome-control-center` and `gnome-initial-setup` and `vanilla-control-center`.

### build:
```bash
#Clone the repo:
git clone https://gitlab.com/zrya/marzi
cd marzi
```

```bash
meson build
cd build
sudo ninja install
```

_Note: This is still heavily-WIP and translations are not setup currently_
